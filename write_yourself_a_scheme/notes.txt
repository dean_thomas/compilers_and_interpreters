Every Haskell program begins with an action called main in a module named Main . That module may import others, but it must be present for the compiler to generate an executable file. Haskell is case-sensitive: module names are always capitalized, declarations always uncapitalized.

The IO type is an instance of something called a monad, which is a scary name for a not-so-scary concept. Basically, a monad is a way of saying “there’s some extra information attached to this value, which most functions don’t need to worry about.” In this example, the “extra information” is the fact that this action performs IO, and the basic value is nothing, represented as (). Monadic values are often called “actions,” because the easiest way to think about the IO monad is a sequencing of actions that each might affect the outside world.

A do-block consists of a series of lines, all lined up with the first non-whitespace character after the do. Each line can have one of two forms:
    1. name <− action
    2. action

Following typical Haskell convention, Parsec returns an Either data type, using the Left constructor to indicate an error and the Right one for a normal value.
